import { ipcMain, webContents } from 'electron'
import log from 'electron-log'
import settings from 'electron-settings'
//
import * as filesApi from '../ipfs/files'

import pjson from '../../../package.json'

// SUBSCRIPTIONS will map all the callback available with a specific method
// {'method': callback }
const SUBSCRIPTIONS = {}

/**
 * Setup the shell <-> spa communication using electron's IPC protocol
 * We need a reference to the main window to be able to reply to requests.
 * Should be called right after we create the main window.
 *
 * Usage example:
 * app.mainWindow = MainWindow.create(app)
 * init(app.mainWindow)
 *
 * Now the shell can receive requests from `electron.ipcRenderer`
 * `ipcRenderer.send('message', { method: 'file/add-path', value: 'Qm...' })`
 *
 */

/**
 * Send a message to the SPA and wait for the response
 *
 * @param {any} message
 * @returns {Promise<any>}
 */
export function requestSPA (request) {
  return new Promise(resolve => {
    // listen for response
    SUBSCRIPTIONS[request.method] = value => resolve(value)
    // send message
    sendMessage(request)
  })
}

// Listen for async messages from the main window process
ipcMain.on('message', (event, request) => {
  log.debug('[Shell] Message from SPA: ', request)

  /**
   * If there is a listener waiting for this, it means this is not a request,
   * but a response from the SPA
   */
  if (request.method in SUBSCRIPTIONS) {
    return SUBSCRIPTIONS[request.method](request.value)
  }

  const reply = responseValue => {
    sendMessage({
      method: request.method,
      value: responseValue
    })
  }

  /**
   * If the request is coming from within the shell, e.g. the menu (add file/ import)
   * pass it to the SPA
   */
  if (request.requestForSPA) {
    return reply(request.value)
  }
  switch (request.method) {
    case 'file/add-path': {
      return filesApi.addFileFromPath(request.value).then(() => reply('Adding completed'))
    }
    case 'ipfs-client/reset': {
      return // filesApi.changeIpfsAddress(request.value).then(() => reply('IPFS re-initialized with the new address'))
    }
    case 'shell/useLocalUI': {
      log.warn('[Shell] User forced to use local UI')
      global.UI_HTTP_ADDRESS = `http://127.0.0.1:8080/ipfs/${pjson.ui.cid}`
      global.UI_SKIP_FETCHING_LATEST = true
      return
    }
    case 'shellSettings/get': {
      reply(settings.getAll())
      return
    }
    case 'shellSettings/set': {
      settings.set(request.key, request.value)
      reply('Set Successfully')
      return
    }
    default: {
      return reply(`Method not supported: ${request.method}`)
    }
  }
})

// Send an async message to the main window process
export function sendMessage (message) {
  log.debug('[Shell] Sending to SPA: ', message)
  // Send the message to all web contents
  webContents.getAllWebContents().forEach((content) => {
    content.send('message', message)
  })
}

export function addActivity (activity) {
  sendMessage({
    method: 'activities/add',
    value: activity
  })
}

export function patchActivity (activity) {
  sendMessage({
    method: 'activities/patch',
    value: activity
  })
}

export function getSettings () {
  return requestSPA({ method: 'settings/get' })
}

export function loadedShellSettings () {
  return requestSPA({ method: 'shellSettings/loaded', settings: settings.getAll() })
}
