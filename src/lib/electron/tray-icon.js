import path from 'path'
import { app, Menu, Tray, nativeImage } from 'electron'

const iconsBasePath = path.join(__dirname, '../../../assets')

/**
 * This will setup a tray icon for the app,
 * which allows the user to:
 *  - open the main window (StorageWindow)
 *  - quit the app.
 */
function setupTrayIcon (customApp) {
  // On macOS the app will stay in the Dock, so no need to trayicon
  if (process.platform === 'darwin') return

  customApp = customApp || app

  const iconPath = `${iconsBasePath}/trackbar-white.png`
  // if (process.platform === 'win32') iconPath = `${iconsBasePath}/logo.ico`

  const icon = nativeImage.createFromPath(iconPath)
  const appTray = new Tray(icon)
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Open Siderus Orion',
      click () {
        customApp.emit('activate')
      }
    },
    {
      label: 'Quit',
      click () {
        customApp.quit()
      }
    }
  ])

  appTray.setToolTip('Siderus Orion')
  // Call this again for Linux because we modified the context menu
  appTray.setContextMenu(contextMenu)

  // When clicking on it activate the main window
  appTray.on('click', () => {
    customApp.emit('activate')
  })

  return appTray
}

export default setupTrayIcon
