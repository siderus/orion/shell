import { getIpfsApi } from './api'
import log from 'electron-log'

/**
 * connectTo allows easily to connect to a node by specifying a str multiaddress
 * example: connectTo("/ip4/192.168.0.22/tcp/4001/ipfs/Qm...")
 * @returns {Promise<>}
 */
export function connectTo (strMultiddr, apiMultiaddress) {
  return getIpfsApi(apiMultiaddress)
    .then(ipfs_client => ipfs_client.swarm.connect(strMultiddr))
    .catch(log.error)
}

/**
 * add a node to the bootstrap list by specifying a str multiaddress,
 * to easily connect to it everytime the daemon starts
 * example: addBootstrapAddr("/ip4/192.168.0.22/tcp/4001/ipfs/Qm...")
 * @returns {Promise<>}
 */
export function addBootstrapAddr (strMultiddr, apiMultiaddress) {
  return getIpfsApi(apiMultiaddress)
    .then(ipfs_client => ipfs_client.bootstrap.add(strMultiddr))
    .catch(log.error)
}
