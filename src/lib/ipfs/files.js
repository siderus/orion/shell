import byteSize from 'byte-size'
import uuidv4 from 'uuid/v4'
import getSize from 'get-folder-size'
import log from 'electron-log'
import { basename } from 'path'
//
import * as ipc from '../electron/ipc'
import { getIpfsApi } from './api'

const CHUNK_SIZE = 262144

/*
 * This is the only place where there is ipfs-api (ipfs-http-client) because
 * most of the interactions are left inside the single page application or in
 * the worker. This implementation is here to support adding and removing files
 * directly from the node process instead of the web worker.
 */

export function addFileFromPath (filePath) {
  log.info(`[addFileFromPath] Adding ${filePath}`)

  // if filePath is not defined return as this should
  if (!filePath) {
    return Promise.reject(`File or Directory not specified. Got: ${filePath}`)
  }

  const name = basename(filePath)
  return new Promise((resolve, reject) => {
    getSize(filePath, (err, size) => {
      if (err) {
        return reject(err)
      }
      // totalSize is used to calculate the progress percentage
      const totalSize = size

      const activity = {
        uuid: uuidv4(),
        path: filePath,
        name,
        type: 'file/add',
        status: 'in progress',
        size: {
          bytes: size,
          ...byteSize(size)
        },
        progress: 0
      }

      ipc.addActivity(activity)

      /**
       * if we are adding a directory we won't get the progress of it
       */
      let cumulativeProgress = 0
      let previousPercentage = 0
      return ipc.getSettings().then(settings =>
        getIpfsApi(settings.ipfsApiAddress)
          .then(ipfs_client => {
            const options = {
              recursive: true,
              wrapWithDirectory: !settings.disableWrapping,
              /**
               * progress is returned for each file, without knowing when a file progress ends
               * we must calculate the sum ourselves
               *
               * example (uploading go-ipfs dir v0.4.17):
               * ```
               * 860 <- new file (install.sh)
               * 262144 <- new file (ipfs bin)
               * 524288 <- same file
               * 786432 <- same file
               * ...
               * 29559344 <- same file
               * 1083 <- new file (LICENSE)
               * 467 <- new file (README.md)
               * ```
               *
               * Note: cumulativeProgress will never equal the actual size
               * because progress is not returned on directories.
               * Do not rely on it to calculate when it's completed!
               */
              progress: (fileProgress) => {
                // ipfs returns 262144, 524288, 786432 for a file that is 786432 bytes big
                // which is why we must only add the diff (the new chunk) and not `fileProgress`
                cumulativeProgress += ((fileProgress - 1) % CHUNK_SIZE) + 1
                const fullPercentage = cumulativeProgress / totalSize
                // ensure it is a number between 0.01 and 1.00
                const percentage = Math.floor(fullPercentage * 100) / 100
                // Prevent sending a lower or the same percentage
                if (percentage <= previousPercentage) return

                // Prevent flooding of events by updating the activities every
                // 5%. This is failing the Worker due to the massive data
                if (((percentage * 100) % 5) !== 0) return

                previousPercentage = percentage
                ipc.patchActivity({
                  uuid: activity.uuid,
                  status: 'in progress',
                  progress: percentage
                })
              }
            }
            return ipfs_client.addFromFs(filePath, options)
          })
          .then((result) => {
            let interest = result
            if (Array.isArray(result)) {
              interest = result[result.length - 1]
            }

            ipc.patchActivity({
              uuid: activity.uuid,
              status: 'completed',
              link: `/properties/${interest.hash}`
            })
          })
          .then(resolve)
          .catch((err) => {
            log.error(`[addFileFromPath] Error importing ${name}: ${err}`)
            reject(err)
          })
      )
    })
  })
}

/**
 * Add a File/directory from Path locally. similar to addFileFromPath, but
 * forcing to add the file locally. Used by the CLI, as it can't get the
 * Settings from it.
 * @param {string} fullPath
 */
export function addFileLocallyFromPath (fullPath, options = { recursive: true, wrapWithDirectory: true }) {
  return getIpfsApi('/ip4/127.0.0.1/tcp/5001')
    .then(ipfs_client =>
      ipfs_client.addFromFs(fullPath, options)
    )
}
