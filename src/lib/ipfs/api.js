import ipfsAPI from 'ipfs-http-client'
import { getSettings } from '../electron/ipc'
import log from 'electron-log'

/**
 * Initialize a new ipfs-api (ipfs-http-client) instance and return it in a promise.
 *
 * Example: `getIpfsApi().then((api) => log.info(api.id()) )`
 *
 * @returns {Promise<IPFS_API>}
 */
export function getIpfsApi (apiMultiaddress) {
  if (apiMultiaddress) return getIpfsApiWithAddress(apiMultiaddress)

  return getSettings()
    .then(settings => {
      return getIpfsApiWithAddress(settings.ipfsApiAddress)
    })
}

/**
 * Similar to getIpfsApi but it doesn't require a window to be open but needs
 * the IPFS multiaddress of the node
 *
 * @returns {Promise<IPFS_API>}
 */

export function getIpfsApiWithAddress (multiaddress) {
  const ipfs_api = ipfsAPI(multiaddress)
  return Promise.resolve(ipfs_api)
}

/**
 *
 * Returns the version of the currently running API.
 * If no API is available it throws.
 *
 * Example: 'v0.4.14'
 * @returns Promise<string>
 */
export function getAPIVersion () {
  log.debug('[getAPIVersion]')

  return getIpfsApi(global.IPFS_MULTIADDR_API)
    .then(ipfs_client => ipfs_client.version())
    .then(res => {
      /**
       * ApiVersionResult {
       *   Version: '0.4.14',
       *   Commit: '',
       *   Repo: '6',
       *   System: 'amd64/linux',
       *   Golang: 'go1.10'
       * }
       */
      return Promise.resolve(`v${res.Version}`)
    })
}

export function isAPIRunning () {
  return new Promise((resolve) =>
    getAPIVersion()
      .then(() => resolve(true))
      .catch(() => resolve(false))
  )
}

/**
 * Wait for the ipfs api to be live.
 * @param {number} timeout defaults to 30 tries, (60 seconds max)
 */
export function waitForAPIReady (timeout = 30) {
  log.info('[waitForAPIReady] Waiting for API')
  let iID // interval id
  let trial = 0
  return new Promise((resolve, reject) => {
    iID = setInterval(() => {
      trial++
      if (trial >= timeout) {
        clearInterval(iID)
        return reject('TIMEOUT')
      }

      return isAPIRunning()
        .then((isRunning) => {
          // if the API are not running return
          if (isRunning !== true) return

          clearInterval(iID)
          log.info('[waitForAPIReady] API Ready')
          return resolve()
        }).catch(() => {
          log.debug('[waitForAPIReady] Waiting for the api to be live...')
        })
    }, 2 * 1000) // every 2 seconds
  })
}
