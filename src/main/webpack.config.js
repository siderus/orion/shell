const path = require("path")
const webpack = require('webpack')

module.exports = {
  target: "electron-main",
  entry: {
    main: path.resolve(__dirname, 'index.js'),
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: '[name].js',
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
	module: {
    rules: [
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        },
      },
    ]
  },
	plugins: [
    new webpack.BannerPlugin({
      banner: 'Copyright (c) 2015-2020 Siderus OU - [name] [hash] - [file]'
    })
  ],
  optimization: {
    runtimeChunk: 'single',
  }
}