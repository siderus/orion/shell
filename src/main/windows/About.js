import path from 'path'
import url from 'url'
import log from 'electron-log';

import { BrowserWindow } from 'electron'

let titleBarStyle = 'default'
if (process.platform === 'darwin') {
  titleBarStyle = 'hiddenInset'
}

const isProduction = process.env.NODE_ENV !== "development";

export default function() {
  log.debug('[AboutWindow] Creating')
  // Create the browser modal window.
  let thisWindow = new BrowserWindow({
    width: 460,
    minWidth: 460,
    height: 320,
    minHeight: 320,
    titleBarStyle,
    maximizable: false,
    minimizable: false,
    resizable: false,
    fullscreenable: false,
    icon: path.join(__dirname, '../assets/icon.png'),

    show: false,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // Show the menu only on the main window
  thisWindow.setMenu(null)

  // Show the window only when ready
  thisWindow.once('ready-to-show', () => {
    log.debug('[AboutWindow] Ready to show')
    setTimeout(() => { thisWindow.show() }, 250)
  })

  if (!isProduction) {
    thisWindow.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}/about.html`)
  } else {
    thisWindow.loadURL(url.format({
      pathname: path.join(__dirname, '../app/about.html'),
      protocol: 'file:',
      slashes: true,
    }));
  }
  // Emitted when the window is closed.
  thisWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    thisWindow = null
  })

  return thisWindow
}
