import path from 'path';
import url from 'url';
import log from 'electron-log';

import { BrowserWindow } from 'electron';

const isProduction = process.env.NODE_ENV !== "development";

export default function create() {
  log.debug('[LoadingWindow] Creating', process.env.NODE_ENV);

  // Create the browser modal window.
  let thisWindow = new BrowserWindow({
    width: 500,
    minWidth: 500,
    height: 340,
    minHeight: 340,

    frame: false,
    maximizable: false,
    resizable: false,
    fullscreenable: false,
    icon: path.join(__dirname, '../../../assets/icon.png'),

    show: false,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // Show the menu only on the main window
  thisWindow.setMenu(null);

  // Show the window only when ready
  thisWindow.once('ready-to-show', () => {
    log.debug('[LoadingWindow] Ready to show');
    setTimeout(() => { thisWindow.show(); }, 250);
  });
  if (!isProduction) {
    log.debug('[LoadingWindow] In dev mode');
    thisWindow.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}/loading.html`)
  } else {
    thisWindow.loadURL(url.format({
      pathname: path.join(__dirname, '../app/loading.html'),
      protocol: 'file:',
      slashes: true,
    }));
  }

  // Emitted when the window is closed.
  thisWindow.on('closed', () => {
    log.debug('[LoadingWindow] Closed');
    // Removes  the progress bar
    thisWindow.setProgressBar(-1);
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    thisWindow = null;
  });

  log.debug('[LoadingWindow] Created');
  return thisWindow;
}
