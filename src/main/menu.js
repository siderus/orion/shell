/**
 * Import/require this file to set up the Menu of the windows
 */

import electron, { BrowserWindow, Menu, app, ipcMain, dialog } from 'electron'
import { checkForUpdates } from '../lib/electron/autoupdate'
import AboutWindow from './windows/About'

const template = [{
  label: 'File',
  submenu: [{
    label: 'Add File',
    click () {
      // Ask the user to choose one or more files
      const options = {
        title: 'Add File',
        properties: ['openFile', 'multiSelections']
      }

      const paths = dialog.showOpenDialog(options)
      if (!paths) return
      paths.forEach(path => {
        // request the shell to add the path, just like the SPA would
        ipcMain.emit('message', null, { method: 'file/add-path', value: path })
      })
    }
  }, {
    label: 'Add Directory',
    click () {
      // Ask the user to choose one or more directorie
      const options = {
        title: 'Add Directory',
        properties: ['openDirectory', 'multiSelections']
      }

      const paths = dialog.showOpenDialog(options)
      if (!paths) return

      paths.forEach(path => {
        // request the shell to add the path, just like the SPA would
        ipcMain.emit('message', null, { method: 'file/add-path', value: path })
      })
    }
  }]
}, {
  label: 'Edit',
  submenu: [{
    label: 'Undo',
    accelerator: 'CmdOrCtrl+Z',
    role: 'undo'
  }, {
    label: 'Redo',
    accelerator: 'Shift+CmdOrCtrl+Z',
    role: 'redo'
  }, {
    type: 'separator'
  }, {
    label: 'Cut',
    accelerator: 'CmdOrCtrl+X',
    role: 'cut'
  }, {
    label: 'Copy',
    accelerator: 'CmdOrCtrl+C',
    role: 'copy'
  }, {
    label: 'Paste',
    accelerator: 'CmdOrCtrl+V',
    role: 'paste'
  }, {
    label: 'Select All',
    accelerator: 'CmdOrCtrl+A',
    role: 'selectall'
  }]
}, {
  label: 'View',
  submenu: [{
    label: 'Reload',
    accelerator: 'CmdOrCtrl+R',
    click (item, focusedWindow) {
      if (focusedWindow) {
        // on reload, start fresh and close any old
        // open secondary windows
        if (focusedWindow.id === 1) {
          BrowserWindow.getAllWindows().forEach((win) => {
            if (win.id > 1) {
              win.close()
            }
          })
        }
        focusedWindow.reload()
      }
    }
  }, {
    label: 'Toggle Developer Tools',
    accelerator: (function () {
      if (process.platform === 'darwin') {
        return 'Alt+Command+I'
      }
      return 'Ctrl+Shift+I'
    }()),
    click (item, focusedWindow) {
      if (focusedWindow) {
        focusedWindow.toggleDevTools()
      }
    }
  }, {
    type: 'separator'
  }]
}, {
  label: 'Window',
  role: 'window',
  submenu: [{
    label: 'Minimize',
    accelerator: 'CmdOrCtrl+M',
    role: 'minimize'
  }, {
    label: 'Close',
    accelerator: 'CmdOrCtrl+W',
    role: 'close'
  }, {
    type: 'separator'
  }, {
    label: 'Reopen Window',
    accelerator: 'CmdOrCtrl+Shift+T',
    enabled: false,
    key: 'reopenMenuItem',
    click () {
      app.emit('activate')
    }
  }]
}, {
  label: 'Help',
  role: 'help',
  submenu: [{
    label: 'Chat with us',
    click () {
      electron.shell.openExternal('https://matrix.to/#/#siderus-orion:matrix.org')
    }
  }, {
    label: 'Suggest new feature',
    click () {
      electron.shell.openExternal('https://github.com/Siderus/Orion/issues/new?template=Feature_request.md')
    }
  }, {
    label: 'Report a bug',
    click () {
      electron.shell.openExternal('https://github.com/Siderus/Orion/issues/new?template=Bug_report.md')
    }
  }, {
    label: 'Learn More',
    click () {
      electron.shell.openExternal('https://orion.siderus.io')
    }
  }]
}]

function addUpdateMenuItems (items, position) {
  if (process.mas) return

  const updateItems = [{
    label: 'Check for Update',
    visible: true,
    enabled: true,
    key: 'checkForUpdate',
    click () {
      checkForUpdates()
    }
  }]

  items.splice(...[position, 0].concat(updateItems))
}

function findReopenMenuItem () {
  const menu = Menu.getApplicationMenu()
  if (!menu) return

  let reopenMenuItem
  menu.items.forEach((item) => {
    if (item.submenu) {
      item.submenu.items.forEach((subItem) => {
        if (subItem.key === 'reopenMenuItem') {
          reopenMenuItem = subItem
        }
      })
    }
  })
  return reopenMenuItem
}

const aboutOrion = {
  label: 'About Orion',
  click() {
    AboutWindow(app);
  },
};

if (process.platform === 'darwin') {
  let name = 'Orion'
  if (electron.app.getName) {
    name = electron.app.getName()
  }

  template.unshift({
    label: name,
    submenu: [aboutOrion, {
      type: 'separator'
    }, {
      label: 'Services',
      role: 'services',
      submenu: []
    }, {
      type: 'separator'
    }, {
      label: `Hide ${name}`,
      accelerator: 'Command+H',
      role: 'hide'
    }, {
      label: 'Hide Others',
      accelerator: 'Command+Alt+H',
      role: 'hideothers'
    }, {
      label: 'Show All',
      role: 'unhide'
    }, {
      type: 'separator'
    }, {
      label: 'Quit',
      accelerator: 'Command+Q',
      click () {
        app.quit()
      }
    }]
  })

  // Window menu.
  template[3].submenu.push({
    type: 'separator'
  }, {
    label: 'Bring All to Front',
    role: 'front'
  })

  addUpdateMenuItems(template[0].submenu, 1)
} else {
  // on windows and linux this menu goes under Help
  template[4].submenu.unshift(aboutOrion)
}

if (process.platform === 'win32') {
  const helpMenu = template[template.length - 1].submenu
  addUpdateMenuItems(helpMenu, 0)
}

app.on('ready', () => {
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)
})

app.on('browser-window-created', () => {
  const reopenMenuItem = findReopenMenuItem()
  if (reopenMenuItem) reopenMenuItem.enabled = false
})

app.on('window-all-closed', () => {
  const reopenMenuItem = findReopenMenuItem()
  if (reopenMenuItem) reopenMenuItem.enabled = true
})
