import { execFile, execFileSync } from 'child_process'
import net from 'net'
import log from 'electron-log'
import settings from 'electron-settings'

import {
  readFileSync,
  writeFile,
  createWriteStream,
  existsSync
} from 'fs'

import { join as pathJoin } from 'path'
import { fileSync as tmpFileSync } from 'tmp'
import { app, dialog } from 'electron'
import { isAPIRunning } from '../lib/ipfs/api'

/**
 * shouldStartDaemon will check the API and see if we need to start a new IPFS
 * daemon instance. Returns a boolean Promise and logs the global IPFS config.
 */
export function shouldStartDaemon () {
  log.debug('[shouldStartDaemon]')

  return isAPIRunning()
    .then(isRunning => {
      // An api is already available on port 5001
      if (isRunning === true) {
        log.warn('[shouldStartDaemon] Existing IPFS API on 127.0.0.1:5001 has been detected')
        global.IPFS_BINARY_PATH = 'ipfs'
        global.IPFS_REPO_PATH = ''
        log.warn('[shouldStartDaemon] IPFS Daemon already running')
      }

      // Logs the path and configuration used
      log.info('[shouldStartDaemon] IPFS_BINARY_PATH', global.IPFS_BINARY_PATH)
      log.info('[shouldStartDaemon] REPO_MIGRATIONS_BINARY_PATH', global.REPO_MIGRATIONS_BINARY_PATH)
      log.info('[shouldStartDaemon] IPFS_MULTIADDR_API', global.IPFS_MULTIADDR_API)
      log.info('[shouldStartDaemon] IPFS_MULTIADDR_GATEWAY', global.IPFS_MULTIADDR_GATEWAY)
      log.info('[shouldStartDaemon] IPFS_MULTIADDR_SWARM', global.IPFS_MULTIADDR_SWARM)
      log.info('[shouldStartDaemon] IPFS_REPO_PATH', global.IPFS_REPO_PATH)

      // it should return the opposite of isRunning
      return Promise.resolve(!isRunning)
    })
}

/**
 * Spawn a new process using the given IPFS command,
 * without needing to specify the binary path or repo path.
 *
 * Example: `const daemonProcess = spawnIPFSCommand('daemon', '--debug')`
 *
 * @param {string} command
 * @returns ChildProcess
 */
export function spawnIPFSCommand (...args) {
  let options
  if (global.IPFS_REPO_PATH.length > 0) {
    options = { env: { IPFS_PATH: global.IPFS_REPO_PATH } }
  }
  log.debug('[SpawnIPFSCmd]', global.IPFS_BINARY_PATH, args, options)

  return execFile(global.IPFS_BINARY_PATH, args, options, (error) => {
    if (error) {
      log.warn('[SpawnIPFSCmd]', global.IPFS_BINARY_PATH, error)
    }
  })
}

/**
 * startIPFSDaemon will start IPFS go daemon, if installed.
 * return a promise with child process of IPFS daemon.
 * The daemon always has 2 options, one to ensure that the repo is initalized
 * the other one to ensure that the api endpoint is the right multiaddr
 */
export function startIPFSDaemon () {
  log.debug('[startIPFSDaemon]')
  return new Promise((resolve, reject) => {
    const args = [
      '--init',
      '--enable-pubsub-experiment',
      `--api=${global.IPFS_MULTIADDR_API}`
    ]
    if (!settings.get('disableDhtClient')) {
      args.push(`--routing=${settings.get('dhtclient') || 'dhtclient'}`)
    }

    const ipfsProcess = spawnIPFSCommand('daemon', ...args)

    if (ipfsProcess === undefined) {
      log.error('[startIPFSDaemon] Undefined IPFS Process. Unable to spawn')
      return reject('Unable to spawn process')
    }

    // Prepare temporary file for logging:
    const tmpLog = tmpFileSync({ keep: true })
    const tmpLogPipe = createWriteStream(tmpLog.name)

    log.debug(`[startIPFSDaemon] Logging IPFS Daemon logs in: ${tmpLog.name}`)

    ipfsProcess.stdout.on('data', (data) => log.debug(`[startIPFSDaemon] IPFS Daemon: ${data}`))
    ipfsProcess.stdout.pipe(tmpLogPipe)

    ipfsProcess.stderr.on('data', (data) => log.error(`[startIPFSDaemon] IPFS Daemon Error: ${data}`))
    ipfsProcess.stderr.pipe(tmpLogPipe)

    ipfsProcess.on('close', (exit) => {
      if (exit !== 0 && exit !== null) {
        let msg = `IPFS Daemon was closed with exit code ${exit}. `
        msg += 'The app will be closed. Try again. '
        msg += `Log file: ${tmpLog.name}`

        dialog.showErrorBox('IPFS was closed, the app will quit', msg)
        app.quit()
      }
      log.warn(`[startIPFSDaemon] IPFS Closed: ${exit}`)
    })

    resolve(ipfsProcess)
  })
}

/**
 * isIPFSInitialised returns a boolean if the repository config file is present
 * in the default path (i.e. userData)
 * (~/.config/Electron/.ipfs/config) in development
 *
 * https://github.com/electron/electron/blob/master/docs/api/app.md#appgetpathname
 */
export function isIPFSInitialised () {
  const confFile = pathJoin(global.IPFS_REPO_PATH, './config')
  log.debug(`[isIPFSInitialised] ${confFile}`)
  return existsSync(confFile)
}

/**
 * ensuresIPFSInitialised will ensure that the repository is initialised
 * correctly in the home directory (by running `ipfs init`)
 */
export function ensuresIPFSInitialised () {
  if (isIPFSInitialised()) return Promise.resolve()

  log.debug('[ensuresIPFSInitialised]')
  return new Promise((resolve, reject) => {
    const ipfsInit = spawnIPFSCommand('init')

    if (ipfsInit === undefined) {
      log.error('[ensuresIPFSInitialised] Undefined IPFS Process. Unable to spawn')
      return reject('Unable to start IPFS init repo')
    }

    ipfsInit.stdout.on('data', (data) => log.debug(`[ensuresIPFSInitialised] IPFS Daemon: ${data}`))
    ipfsInit.stderr.on('data', (data) => log.error(`[ensuresIPFSInitialised] IPFS Daemon Error: ${data}`))

    ipfsInit.on('close', (exit) => {
      if (exit !== 0 && exit !== null) {
        reject('Unable to initialize IPFS repo')
      }
      resolve()
    })
  })
}

/**
 * This will ensure IPFS Daemon starts on the correct ports by changing the
 * API, Gateway and Address ports to the one specified in the global variables
 *
 * @returns Promise
 */
export function ensureDaemonConfigured () {
  log.debug('[ensureDaemonConfigured]')
  return new Promise((resolve, reject) => {
    // Read the json conifugation. A simple require() won't work
    const configFilePath = pathJoin(global.IPFS_REPO_PATH, './config')
    const confRaw = readFileSync(configFilePath, { encoding: 'utf8' })
    const conf = JSON.parse(confRaw)

    // Change the configuration
    conf.Addresses.API = global.IPFS_MULTIADDR_API
    conf.Addresses.Gateway = global.IPFS_MULTIADDR_GATEWAY
    conf.Addresses.Swarm = global.IPFS_MULTIADDR_SWARM
    conf.API.HTTPHeaders['Access-Control-Allow-Origin'] = global.IPFS_API_CORS_POLICY

    // Return a promise that writes the configuration back in the file
    writeFile(configFilePath, JSON.stringify(conf, null, 2), function (err) {
      if (err) return reject(err)
      return resolve()
    })
  })
}

/**
 * This will ensure the ipfs repo version is compatible with the daemon
 * by running migrations on the ipfs repo
 */
export function ensureRepoMigrated () {
  log.debug('[ensureRepoMigrated]')
  return new Promise((resolve, reject) => {
    let options
    if (global.IPFS_REPO_PATH.length > 0) {
      options = { env: { IPFS_PATH: global.IPFS_REPO_PATH } }
    }

    log.debug(`[Daemon] Running "${global.REPO_MIGRATIONS_BINARY_PATH} -y"`, options)
    const result = execFileSync(global.REPO_MIGRATIONS_BINARY_PATH, ['-y'], options).toString('utf8')
    log.debug(`[Daemon] Completed "${global.REPO_MIGRATIONS_BINARY_PATH}":`, result)
    resolve()
  })
}

export function ensurePortNotTaken () {
  log.debug('[ensurePortNotTaken]')
  return isPortTaken(5001)
    .then(isPortTaken => {
      if (isPortTaken) {
        log.error('[ensurePortNotTaken] Port is taken')
        let msg = `Siderus Orion tried to start an existing node, `
        msg += 'but it looks like that the port 5001 is already in use.\n'
        msg += 'Solution: before starting Siderus Orion ensure that the port 5001 is not in use '
        msg += 'by other processes or it is used by an IPFS node.'

        dialog.showErrorBox('IPFS can not start, the app will quit', msg)
        return Promise.reject('Port 5001 in use')
      }
    })
}

function isPortTaken (port) {
  return new Promise(resolve => {
    const tester = net.createServer()
    tester.once('error', () => resolve(true))
    tester.once('listening', () => {
      tester.once('close', () => resolve(false))
      tester.close()
    })
    tester.listen(port)
  })
}
