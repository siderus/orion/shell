import React from 'react'
import {render} from 'react-dom'

import About from './About.jsx'
import Loading from './Loading.jsx'

render(
    <Loading/>,
    document.getElementById('app')
)