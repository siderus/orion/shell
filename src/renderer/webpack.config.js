const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  target: 'electron-renderer',
  entry: {
    about: path.resolve(__dirname, 'About.jsx'),
    loading: path.resolve(__dirname, 'Loading.jsx'),
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Orion by Siderus',
      template: path.resolve(__dirname, 'index.html'),
      excludeChunks: ['loading'],
      hash: true,
      filename: 'about.html',
    }),
    new HtmlWebpackPlugin({
      title: 'Orion by Siderus',
      template: path.resolve(__dirname, 'index.html'),
      excludeChunks: ['about'],
      hash: true,
      filename: 'loading.html',
    }),
    new webpack.BannerPlugin({
      banner: 'Copyright (c) 2015-2020 Siderus OU - [name] [hash] - [file]',
    }),
  ],
  optimization: {
    runtimeChunk: {
      name: entrypoint => `runtime~${entrypoint.name}`
    },
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },
  performance: {
    hints: false,
  }
}