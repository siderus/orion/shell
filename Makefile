# we need the go-ipfs binary
IPFS_VERSION := $(shell cat ./package.json | jq .ipfsVersion -r )
IPFS_BINARY_NAME ?= go-ipfs_${IPFS_VERSION}_${TARGET}-${BUILD_ARCH}${BINARY_EXT}
BINARY_URL ?= https://github.com/ipfs/go-ipfs/releases/download/${IPFS_VERSION}/${IPFS_BINARY_NAME}
# BINARY_URL ?= https://dist.ipfs.io/go-ipfs/${IPFS_VERSION}/${IPFS_BINARY_NAME}
GIT_TAG ?= ${shell git tag -l --points-at HEAD}

ifeq ($(GIT_TAG),)
	GIT_TAG := v1.0.0-development
endif

# we need the fs-repo-migrations binary
REPO_MIGRATIONS_VERSION := $(shell cat ./package.json | jq .ipfsRepoMigrationsVersion -r )
REPO_MIGRATIONS_BINARY_NAME ?= fs-repo-migrations_${REPO_MIGRATIONS_VERSION}_${TARGET}-${BUILD_ARCH}${BINARY_EXT}
REPO_MIGRATIONS_BINARY_URL ?= https://dist.ipfs.io/fs-repo-migrations/${REPO_MIGRATIONS_VERSION}/${REPO_MIGRATIONS_BINARY_NAME}
NODE_ENV ?= development
TMP_DIR := $(shell mktemp -d)
YARN_EXTRA_ARGS ?= --prefer-offline --pure-lockfile

# Chunk the v at first char from the git tag
_VERSION := $(shell echo $(GIT_TAG) | sed 's/^\(.\{0\}\)v//g')
# Extract major and minor version, used by subdomains
_MAJMIN_VERSION := $(shell echo "${_VERSION}" | cut -d'.' -f 1,2)
# Extract the channel, if none provided it will equal to _VERSION
_CHANNEL := $(shell echo "${_VERSION}" | sed 's/^.*-//g')
# Defines the subdomain to use for content distribution
SUB_DOMAIN ?=  $(shell echo "${_CHANNEL}" | sed 's/\./-/g' ).

# If we don't have a channel in the version (ex: 1.2.3-beta has beta channel)
ifeq ($(_CHANNEL),$(_VERSION))
	_CHANNEL := production
	SUB_DOMAIN := $(shell echo "${_MAJMIN_VERSION}" | sed 's/\./-/g' ).
endif

# The development channel doesn't exist on subdomains. Use beta instead
ifeq ($(SUB_DOMAIN),development.)
	SUB_DOMAIN := beta.
endif

GH_TOKEN ?=
SNAPCRAFT_TOKEN ?=
BUILD_ARCH ?= amd64

ifeq ($(BUILD_ARCH), 386)
	BUILD_ARGS += --ia32
endif

ifeq ($(OS),Windows_NT)
	BINARY_EXT := .zip
	DECOMPRESSOR := unzip
	TARGET := windows
	BUILD_ARGS += --windows
else
	BINARY_EXT := .tar.gz
	DECOMPRESSOR := tar xf
	UNAME_S ?= $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		TARGET := linux
		BUILD_ARGS += --linux --config.extraMetadata.name=siderus-orion
	endif
	ifeq ($(UNAME_S),Darwin)
		TARGET := darwin
		BUILD_ARGS += --mac --x64
	endif
endif

_test_variables:
	@test -n "$(GIT_TAG)" || (echo "Variable CURRENT_VERSION not set"; exit 1)
	@test -n "$(TARGET)" || (echo "Variable TARGET not set"; exit 1)
	@test -n "$(NODE_ENV)" || (echo "Variable TARGET not set"; exit 1)
	@test -n "$(IPFS_VERSION)" || (echo "Variable IPFS_VERSION not set"; exit 1)
	@test -n "$(BINARY_URL)" || (echo "Variable BINARY_URL not set"; exit 1)
	@test -n "$(REPO_MIGRATIONS_VERSION)" || (echo "Variable REPO_MIGRATIONS_VERSION not set"; exit 1)
	@test -n "$(REPO_MIGRATIONS_BINARY_URL)" || (echo "Variable REPO_MIGRATIONS_BINARY_URL not set"; exit 1)
	@test -n "$(DECOMPRESSOR)" || (echo "Variable DECOMPRESSOR not set"; exit 1)
	@test -n "$(BUILD_ARGS)" || (echo "Variable BUILD_ARGS not set"; exit 1)
.PHONY: _test_variables

clean:
	rm -rf .cache
	rm -rf build
	rm -rf dist
	rm -rf go-ipfs
	rm -rf coverage
	rm -rf node_modules
	rm -rf repo
	rm -rf fs-repo-migrations
	rm -rf assets/ui
.PHONY: clean

# Deletes every sub-directory from build directory (like unpacked files)
purge_build_directory:
	find ./build/ -maxdepth 1 -mindepth 1 -type d -exec rm -rf '{}' \;
.PHONEY: purge_build_directory

# Yarn packages, tests and linting
dep:
	NODE_ENV=development yarn --link-duplicates ${YARN_EXTRA_ARGS}
.PHONY: dep
dependencies: dep
.PHONEY: dependencies

run: dep
	test -s go-ipfs/ipfs || "$(MAKE)" prepare_ipfs_bin
	test -s fs-repo-migrations/fs-repo-migrations || "$(MAKE)" prepare_repo_migrations_bin
	rm -rf .cache
	yarn run electron-webpack dev
.PHONY: run

lint: dep
	yarn lint
.PHONY: lint

_test:
	yarn test
.PHONEY: _test

_test%:
	yarn test$*
.PHONY: _test

test: prepare_repo_migrations_bin prepare_ipfs_bin dep _test _test-integration
.PHONY: test

test_release: prepare_release test
.PHONY: test_release

test_ci: dep _test
.PHONY: test_ci

# Building the packages for multiple platforms
build_icons: dep
	./node_modules/.bin/icon-gen -i ./assets/icon.svg -o ./assets/ -m ico,icns -n ico=logo,icns=logo
.PHONY: build_icons

_prepkg: dep _test_variables
	mkdir -p build
	cp ./assets/logo.icns ./build/
	cp ./assets/logo.ico ./build/
.PHONY: _prepkg

# This target will set the version according to the current variable (default TAG)
_prepare_version: _test_variables
	cat package.json | sed "s/\"v1.0.0-development\",$$/\"${GIT_TAG}\",/g" >> package.new.json
	mv package.new.json package.json
.PHONEY: _prepare_version

# This endpoints updatse package.json in order to make them realease ready!
prepare_release: _prepare_version
	cat package.json | sed "s/\"development\",$$/\"${_CHANNEL}\",/g" >> package.new.json
	mv package.new.json package.json
	cat package.json | sed "s/\"beta.orion.siderus.ipfs.rocks\"/\"${SUB_DOMAIN}orion.siderus.ipfs.rocks\"/g" >> package.new.json
	mv package.new.json package.json
.PHONY: prepare_release

prepare_binaries: prepare_ipfs_bin prepare_repo_migrations_bin
.PHONY: prepare_binaries

# Download the latest IPFS content of the `.ui.cid` value as a tarball archive
prepare_ui: _download_ui _update_ui_cid
.PHONEY: prepare_ui

_download_ui:
	rm -rf ./assets/ui.tar
	ipfs get -a -o ./assets/ui.tar $$(cat ./package.json| jq .ui.cid -r)
.PHONEY: _download_ui

# This will change the content of package.json (.ui.cid) to the latest available
_update_ui_cid:
	@echo "Updating package to use ${SUB_DOMAIN}orion.siderus.ipfs.rocks"
	jq \
		'.ui.cid |= "$(shell ipfs resolve /ipns/${SUB_DOMAIN}orion.siderus.ipfs.rocks | sed "s|/ipfs/||g")"' \
		package.json >> package.new.json
	mv package.new.json package.json
.PHONEY: _update_ui_cid

# Download the go-ipfs binary from the URL
prepare_ipfs_bin: _test_variables
	curl -L -o ./${IPFS_BINARY_NAME} ${BINARY_URL}
	rm -rf ./go-ipfs
	$(DECOMPRESSOR) ${IPFS_BINARY_NAME}
	rm ${IPFS_BINARY_NAME}
.PHONY: prepare_ipfs_bin

# Download the fs-repo-migrations binary from the URL
prepare_repo_migrations_bin: _test_variables
	curl -L -o ./${REPO_MIGRATIONS_BINARY_NAME} ${REPO_MIGRATIONS_BINARY_URL}
	rm -rf ./fs-repo-migrations
	$(DECOMPRESSOR) ${REPO_MIGRATIONS_BINARY_NAME}
	rm ${REPO_MIGRATIONS_BINARY_NAME}
.PHONY: prepare_repo_migrations_bin

compile:
	yarn run electron-webpack
.PHONEY: compile

build_mac:
	$(MAKE) prepare_binaries build -e OS="Darwin" -e UNAME_S="Darwin"
.PHONEY: build_mac

build_win:
	$(MAKE) prepare_binaries build -e OS="Windows_NT"
.PHONEY: build_win

build_gnu:
	$(MAKE) prepare_binaries build -e OS="Linux" -e UNAME_S="Linux"
.PHONEY: build_gnu

build: _test_variables _prepare_version _prepkg compile
	yarn run electron-builder ${BUILD_ARGS}
.PHONY: build

build_all: clean build_mac build_gnu build_win
.PHONY: build_all

release: _test_variables prepare_release prepare_binaries _prepkg
	./node_modules/.bin/build ${BUILD_ARGS} --publish always
.PHONY: release

release_all: clean
	$(MAKE) release -e OS="Darwin" -e UNAME_S="Darwin"
	$(MAKE) release -e OS="Linux" -e UNAME_S="Linux"
	$(MAKE) release -e OS="Windows_NT"
.PHONY: release_all

upload_ipfs: purge_build_directory
	find ./build/ -type f -maxdepth 1 -exec curl -I -X POST --upload-file {} https://siderus.io/ipfs/ \;
.PHONEY: upload_ipfs
